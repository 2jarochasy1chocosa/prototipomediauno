﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{
    public GameObject message;
    private void Start()
    {
        message.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player" && GameManager.instance.letterread == true)
        {
            message.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "player")
        {
            message.SetActive(false);
        }
    }
}
