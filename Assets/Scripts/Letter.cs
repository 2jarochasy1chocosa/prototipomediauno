﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Letter : MonoBehaviour
{
    public GameObject letterbundle;
    public GameObject mepueh; 
    public GameObject zoomText;
    public static Letter instance;

    private void Awake()
    {
        instance = this;
    }

    public void DestroyLetter()
    {
        letterbundle.SetActive(false);
        mepueh.SetActive(false);
    }
}

