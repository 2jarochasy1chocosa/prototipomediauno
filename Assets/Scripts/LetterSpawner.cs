﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterSpawner : MonoBehaviour
{
    public GameObject letter;
    private void Start()
    {
        letter.SetActive(false);
        StartCoroutine(LetterAppears());
    }

    IEnumerator LetterAppears()
    {
        yield return new WaitForSeconds(3f);
        letter.SetActive(true);
    }
}
