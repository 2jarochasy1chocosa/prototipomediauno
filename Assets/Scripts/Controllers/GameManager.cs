﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int interview;
    public int suspects;
    public bool canChangeScene;
    public GameObject CambioDeEscena, Instrucciones;
    public bool displayInstructions, theresSuspects;
    public GameObject[] Bloquedo;
    public int indexBloq;
    public static GameManager instance;
    public int sceneNumber;
    public bool letterread;
    public string[] clues;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        if(displayInstructions == true)
        {
            Instrucciones.SetActive(true);
            Bloquedo[indexBloq].SetActive(false);
            
        }
        CambioDeEscena.SetActive(false);
        
    }
    public void changeScene()
    {
        SceneManager.LoadScene(sceneNumber);
    }

    private void Update()
    {
        if(displayInstructions == true) { 
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Instrucciones.SetActive(false);
                Bloquedo[indexBloq].SetActive(true);
                CambioDeEscena.SetActive(false);
            }
        }
        if(theresSuspects == true) { 
        if (interview == suspects && Question.instance.interviewOver == true)
        {
            canChangeScene = true;
            CambioDeEscena.SetActive(true);
        }
        }
        if (canChangeScene == true && Input.GetKeyDown(KeyCode.E))
        {
            changeScene();

        }
    }
}
