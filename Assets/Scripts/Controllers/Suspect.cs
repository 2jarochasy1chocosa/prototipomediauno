﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suspect : MonoBehaviour
{
    public static Suspect instance;
    public GameObject[] suspects;
    public GameObject suspectManager;
    public GameObject[] interview;
    public bool startInterview;

    public int indexSuspect;
    public int indexAnswers;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        startInterview = false;
        for (int i = 0; i < interview.Length; i++)
        {
            interview[i].SetActive(false);
        }

        interview[indexAnswers].SetActive(false);
    }
    private void Update()
    {
        if (startInterview == true)
        {
            SuspectSelection();
        }
    }
    public void SuspectSelection()
    {

        if (indexSuspect == 0)
        {
            interview[0].SetActive(true);
            suspectManager.SetActive(false);
        }
        if (indexSuspect == 1)
        {
            interview[1].SetActive(true);
            suspectManager.SetActive(false);
        }
        if (indexSuspect == 2)
        {
            interview[2].SetActive(true);
            suspectManager.SetActive(false);
        }
        if (indexSuspect == 3)
        {
            interview[3].SetActive(true);
            suspectManager.SetActive(false);
        }
    }
    public void Suspect1()
    {
        indexSuspect = 0;
        startInterview = true;
    }
    public void Suspect2()
    {
        indexSuspect = 1;
        startInterview = true;
    }
    public void Suspect3()
    {
        indexSuspect = 2;
        startInterview = true;
    }
    public void Suspect4()
    {
        indexSuspect = 3;
        startInterview = true;
    }

}
