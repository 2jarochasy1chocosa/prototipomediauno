﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterTopController : MonoBehaviour
{
    public static CharacterTopController instance;
    public float speed = 4;
    float savespeed;
    bool closeletter; 
    public float blocked = 0;
    private Vector3 screenbounds;
    Quaternion position;
    private Rigidbody rb;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        closeletter = false;
        savespeed = speed;
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY; 
    }
    void Update()
    {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(Vector3.down * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
            }
            if (GameManager.instance.letterread == true && closeletter == true)
            {
                if (Input.GetKey(KeyCode.E))
                {
                    Letter.instance.DestroyLetter();
                    speed = savespeed;
                }
            }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "envelope")
        {
            if (Input.GetKey(KeyCode.E))
            {
                Envelope.instance.letter.SetActive(true);
                Envelope.instance.gameObject.SetActive(false);
                GameManager.instance.letterread = true;
                speed = blocked;
                StartCoroutine(CloseLetter());
            }
        }
        if (other.tag == "exitDoor")
        {
            if (Input.GetKey(KeyCode.E))
            {
                Debug.Log("Exit");
                GameManager.instance.changeScene();
            }
        }
    }
    IEnumerator CloseLetter()
    {
        yield return new WaitForSeconds(1f);
        closeletter = true;

    }
}
