﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseGuilty : MonoBehaviour
{
    public static ChooseGuilty instance;
    public GameObject[] suspects;
    public GameObject[] endings;
    public int seconds;
    public int indexSuspect;
    public int indexEndings;
    public bool guiltySelect;

    private void Awake()
    {
        instance = this;
    }
    public void Start()
    {
        guiltySelect = false;
        suspects[indexSuspect].SetActive(true);
        endings[indexEndings].SetActive(false);
    }
    public void Update()
    {

        if(guiltySelect == true)
        {
            if (Input.GetKeyDown(KeyCode.E) && Dialogue.instance.canChange == true && Dialogue.instance.index == Dialogue.instance.sentences.Length)
            {

                print("CambiodeEscena");
                GameManager.instance.changeScene();
            }
        }
    }
    public void Suspect1()
    {
        indexSuspect = 0;
        if (indexSuspect == 0)
        {
            StartCoroutine(GuiltyChose());
            endings[0].SetActive(true);
        }
    }
    public void Suspect2()
    {
        indexSuspect = 1;
        if (indexSuspect == 1)
        {
            StartCoroutine(GuiltyChose());
            endings[1].SetActive(true);
        }
    }
    public void Suspect3()
    {
        indexSuspect = 2;
        if (indexSuspect == 2)
        {
            StartCoroutine(GuiltyChose());
            endings[2].SetActive(true);
        }
    }
    public void Suspect4()
    {
        indexSuspect = 3;
        if (indexSuspect == 3)
        {
            StartCoroutine(GuiltyChose());
            endings[3].SetActive(true);
        }
    }
    IEnumerator GuiltyChose()
    {
        yield return new WaitForSeconds(seconds);
        guiltySelect = true;
    }
}
