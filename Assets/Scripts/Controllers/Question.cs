﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question : MonoBehaviour
{
    public static Question instance;
    public GameObject questions;
    public GameObject myCard;
    public GameObject suspects;
    public bool twoEnough;
    public GameObject myInterview;
    public bool answers3;
    public bool answers4, interviewOver;
    public int questionsAsked;
    public int twoquestions;
    public bool[] asked;
    public GameObject[] sentences;
    public GameObject[] answer;
    public int indexAsked;
    public int indexAnswer;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        GameManager.instance.interview++;
        asked[indexAsked] = false;
        for (int i = 0; i < answer.Length; i++)
        {
            answer[i].SetActive(false);
        }
        answer[indexAnswer].SetActive(false);
        interviewOver = false;
        
    }
    private void Update()
    {
        if(twoEnough == true)
        {
            if(twoquestions >= 2)
            {
                if (Input.GetKeyDown(KeyCode.Alpha4))
                {
                    print("InterviewCanceled");
                    myCard.SetActive(false);
                    suspects.SetActive(true);
                    myInterview.SetActive(false);
                    Suspect.instance.startInterview = false;
                    interviewOver = true;
                }
            }
        }
        if(answers3 == true)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && asked[0] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[0].SetActive(true);
                answer[1].SetActive(false);
                answer[2].SetActive(false);
                asked[0] = true;
                questions.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && asked[1] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[1].SetActive(true);
                answer[0].SetActive(false);
                answer[2].SetActive(false);
                asked[1] = true;
                questions.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && asked[2] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[2].SetActive(true);
                answer[1].SetActive(false);
                answer[0].SetActive(false);
                asked[2] = true;
                questions.SetActive(false);
            }
        }
        if (answers4 == true)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && asked[0] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[0].SetActive(true);
                answer[1].SetActive(false);
                answer[2].SetActive(false);
                answer[3].SetActive(false);
                asked[0] = true;
                questions.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && asked[1] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[1].SetActive(true);
                answer[0].SetActive(false);
                answer[2].SetActive(false);
                answer[3].SetActive(false);
                asked[1] = true;
                questions.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && asked[2] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[2].SetActive(true);
                answer[1].SetActive(false);
                answer[0].SetActive(false);
                answer[3].SetActive(false);
                asked[2] = true;
                questions.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4) && asked[3] == false)
            {
                twoquestions++;
                questionsAsked++;
                answer[3].SetActive(true);
                answer[1].SetActive(false);
                answer[2].SetActive(false);
                answer[0].SetActive(false);
                asked[3] = true;
                questions.SetActive(false);
            }
        }
    }
    private void LateUpdate()
    {
        if (questionsAsked == answer.Length)
        {
            print("InterviewOver");
            myCard.SetActive(false);
            suspects.SetActive(true);
            myInterview.SetActive(false);
            Suspect.instance.startInterview = false;
            interviewOver = true;
        }
    }
}
