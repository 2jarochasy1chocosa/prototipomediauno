﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{
    public static Dialogue instance;
    public GameObject Questions;
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    public bool canChange;
    public bool changeHere;
    public int index;
    public float typingSpeed = 0.05f;
    private void Start()
    {
        instance = this;
        StartCoroutine(Type());
 
      
    }
    private void Update()
    {
        if(changeHere == true )
        {
            if(Input.GetKey(KeyCode.E) && canChange == true && index >= sentences.Length - 1)
            {
                print("ChangeScene");
                GameManager.instance.changeScene();
            }
        }
        if(textDisplay.text == sentences[index])
        {
            canChange = true;
        }
        if (Input.GetKeyDown(KeyCode.Space) && canChange == true)
        {
            NexSentences();
        }
        if (Input.GetKey(KeyCode.Space))
        {
            typingSpeed = 0.001f;
        }
        else
        {
            typingSpeed = 0.05f;
        }
    }
    IEnumerator Type()
    {
        foreach(char letter in sentences[index].ToCharArray()) {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NexSentences()
    {
        canChange = false; 
        if(index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        } else
        {
            gameObject.SetActive(false);
            Questions.SetActive(true);
            textDisplay.text = "";
        }
    }
}
