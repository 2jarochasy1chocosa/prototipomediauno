﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Type : MonoBehaviour
{
    public static Type instance;
    public float typingSpeed = 0.05f;
    public bool canChange;
    public int index;
    public string[] sentences;
    public TextMeshProUGUI textDisplay;

    private void Start()
    {
        instance = this;
        StartCoroutine(Typing());
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            typingSpeed = 0.001f;
        }
        else
        {
            typingSpeed = 0.05f;
        }
    }
    IEnumerator Typing()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }
    IEnumerator ChangeSentence()
    {
        yield return new WaitForSeconds(1);
        canChange = true;
    }

}
