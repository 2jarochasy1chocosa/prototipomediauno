﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Envelope : MonoBehaviour
{
    public GameObject message;
    public GameObject letter;
    public static Envelope instance;
    public void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        letter.SetActive(false);
        message.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            message.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "player")
        {
            message.SetActive(false);
        }
    }
}
